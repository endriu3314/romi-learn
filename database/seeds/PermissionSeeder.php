<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Delete existing permissions
         */
        // Schema::disableForeignKeyConstraints();

        // DB::table('permissions')->truncate();
        // DB::table('role_has_permissions')->truncate();
        // DB::table('roles')->truncate();

        // Schema::enableForeignKeyConstraints();


        //reset cached roles & permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //create permissions
        Permission::create(['name' => 'view dashboard']);
        Permission::create(['name' => 'manage oauth']);

        //create roles
        Role::create(['name' => 'Member'])->givePermissionTo([
            'view dashboard'
        ]);

        Role::create(['name' => 'Admin'])->givePermissionTo([
            'view dashboard',
            'manage oauth'
        ]);

        //doar de exemplu asta
        Role::create(['name' => 'Master Admin'])->givePermissionTo(Permission::all());
    }
}
