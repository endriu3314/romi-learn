# Init

> CREATE DATABASE

For XAMPP:

-   `cd c:\xampp\mysql\bin`
-   `.\mysql.exe -u root -p`
-   `create database example;`

> LAUNCH PROJECT

-   `composer i`
-   `copy('.env.example', '.env');` (_Change database values_)
-   `php artisan key:generate`
-   `php artisan migrate -seed`
-   `php artisan serve`

# Steps took

> MAKE PROJECT

-   `laravel new example-auth` (_Laravel CLI installed_ `composer global require laravel/installer`)

> MAKE AUTH

-   `composer require laravel/ui`
-   `php artisan ui bootstrap --auth` (_nu folosim vue / react_)
-   `npm i`
-   `npm run dev`
-   `php artisan migrate`

> PASSPORT

-   `composer require laravel/passport`
-   `php artisan migrate`
-   `php artisan passport:install`
-   Trebuie adaugat Trait la modelul de user `App\User`.

    Adauga la model `HasApiTokens`

    ```php
    use Notifiable;
    ```

    ```php
    use HasApiTokens, Notifiable;
    ```

-   In `app/Providers` exista `AuthServiceProvider`, trebuie adaugat `Passport::routes` in metoda de `boot`

    ```php
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
    ```

-   In `config/auth.php` adauga auth pentru `api`

    ```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
            'hash' => false,
        ],
    ],
    ```

    ```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],
    ```

-   E dragut sa ai si ceva vizual despre OAuth dar din pacate is numai in Vue 😄

    -   `php artisan vendor:publish --tag=passport-components`

    -   Instalam Vue prin CLI pentru ca e mai usor 😆 (_e doar 2KB_)

        -   `php artisan ui vue`
        -   `npm i`
        -   `npm run dev`

    -   Cum o sa folosim doar blade pt asta putem sa inregistram componentele de Vue manual.

        In `resources/js/app.js`

        ```js
        Vue.component(
            "passport-clients",
            require("./components/passport/Clients.vue").default
        );

        Vue.component(
            "passport-authorized-clients",
            require("./components/passport/AuthorizedClients.vue").default
        );

        Vue.component(
            "passport-personal-access-tokens",
            require("./components/passport/PersonalAccessTokens.vue").default
        );
        ```

        `npm run dev` (E ultima data nu are rost sa pornesti watch)

    -   Cream fisierul in care adaugam componentele Vue.

        -   `cd .\resources\views\ ; mkdir admin`
        -   `cd .\admin\ ; touch oauth.blade.php`

        -   Adauga in fisierul `admin\oauth.blade.php`

            ```php
            @extends('layouts.app')

            @section('content')
            <passport-clients></passport-clients>
            <passport-authorized-clients></passport-authorized-clients>
            <passport-personal-access-tokens></passport-personal-access-tokens>
            @endsection
            ```

    -   Trebuie sa adaugam ruta in `routes/web.php` (Le protejam mai tarziu)

        ```php
        Route::get('/admin/oauth', function () {
            return view('admin.oauth');
        });
        ```

> ROLES & PERMISSIONS & RUTARE (PUTIN)

#### Toretic in Laravel ai Gates daca vrei sa nu folosesti dependinta dar mie imi place asta si dependintele de la spatie sunt destul de mari si folosite peste tot

-   Instalare

    -   `composer require spatie/laravel-permission`
    -   `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"`
    -   `php artisan optimize:clear` (_Clear cache_)
    -   `php artisan migrate`

-   Eloquent

    -   Trebuie adaugat Trait cum am facut si pentru Passport in `App\User`

    ```php
    use HasRoles, HasApiTokens, Notifiable;
    ```

    -   Venite din Trait

        ```php
        // get a list of all permissions directly assigned to the user
        $permissionNames = $user->getPermissionNames(); // collection of name strings
        $permissions = $user->permissions; // collection of permission objects

        // get all permissions for the user, either directly, or from roles, or from both
        $permissions = $user->getDirectPermissions();
        $permissions = $user->getPermissionsViaRoles();
        $permissions = $user->getAllPermissions();

        // get the names of the user's roles
        $roles = $user->getRoleNames(); // Returns a collection
        ```

    -   Eloquent

        ```php
        $all_users_with_all_their_roles = User::with('roles')->get();
        $all_users_with_all_direct_permissions = User::with('permissions')->get();
        $all_roles_in_database = Role::all()->pluck('name');
        $users_without_any_roles = User::doesntHave('roles')->get();
        ```

-   Primul seeder 😃

    -   `php artisan make:seeder PermissionSeeder`

    -   In `database/seeds/` a aparaut `PermissionSeeder`

        ```php
        <?php

        use Illuminate\Database\Seeder;

        use Spatie\Permission\Models\Permission;
        use Spatie\Permission\Models\Role;

        class PermissionSeeder extends Seeder
        {
            /**
            * Run the database seeds.
            *
            * @return void
            */
            public function run()
            {
                /**
                * Delete existing permissions
                */
                // Schema::disableForeignKeyConstraints();

                // DB::table('permissions')->truncate();
                // DB::table('role_has_permissions')->truncate();
                // DB::table('roles')->truncate();

                // Schema::enableForeignKeyConstraints();

                //reset cached roles & permissions
                app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

                //create permissions
                Permission::create(['name' => 'view dashboard']);
                Permission::create(['name' => 'manage oauth']);

                //create roles
                Role::create(['name' => 'Member'])->givePermissionTo([
                    'view dashboard'
                ]);

                Role::create(['name' => 'Admin'])->givePermissionTo([
                    'view dashboard',
                    'manage oauth'
                ]);

                //doar de exemplu asta
                Role::create(['name' => 'Master Admin'])->givePermissionTo(Permission::all());
            }
        }
        ```

    -   Poti chema doar un seeder folosind optiunea `--class` sau seederul default care le va chema pe toate.

        -   `php artisan db:seed -class=PermissionSeeder`
        -   `php artisan db:seed` (_Cheama DatabaseSeeder, el este root_)
            -   In `database/seeds/DatabaseSeeder.php` trebuie adaugata urmatoarea linie pentru ca oricand folosim `db:seed` seederul nostru sa fie activat:
            -   `$this->call(PermissionSeeder::class);` (_In functia `run()`_)

    -   Protecting routes

        -   `php artisan make:middleware PermissionMiddleware`

        -   Content of `app/Http/Middleware/PermissionMiddleware.php`

            ```php
            <?php

            namespace App\Http\Middleware;

            use Auth;
            use Closure;

            class PermissionMiddleware
            {
                /**
                * Handle an incoming request.
                *
                * @param  \Illuminate\Http\Request  $request
                * @param  \Closure  $next
                * @param  string  $permission
                * @return mixed
                */
                public function handle($request, Closure $next, $permission)
                {
                    if (Auth::guest()) {
                        return redirect()->route('login');
                    }

                    if (!$request->user()->can($permission)) {
                        return redirect('/');
                    }

                    return $next($request);
                }
            }
            ```

        -   Dupa ce facem MiddleWareul trebuie adaugat in `app/Http/Kernel.php` in arrayul `protected $routeMiddleware`

            -   `'permission' => \App\Http\Middleware\PermissionMiddleware::class,`

        -   In `routes/web.php` aveam ruta pentru cheile de OAuth.

            ```php
            Route::get('/admin/oauth', function () {
                return view('admin.oauth');
            });
            ```

            Va devenii: (_Grupam rutele cu middleware create mai sus sa verificam daca userul are permisia_)

            ```php
            Route::group(['middleware' => ['permission: manage oauth']], function () {
                Route::get('/admin/oauth', function () {
                    return view('admin.oauth');
                });
            });
            ```

            Pentru a nu ne mai chinuii sa scriem /admin/ in fata la fiecare ruta din acest grup putem face un grup cu prefix.

            ```php
            Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
                Route::group(['middleware' => ['permission: manage oauth']], function () {
                    Route::get('/oauth', function () {
                        return view('admin.oauth');
                    });
                });
            });
            ```

            Pentru a chema ruta trebuie sa ii adaugam un nume. Deoarece am definit un prefix deasupra numele va fii doar `get` in loc de `admin.get`

            ```php
            ...

            Route::get('/oauth', function () {
                return view('admin.oauth');
            })->name('get-oauth');
            ```

            Chemam ruta cu `admin.get-oauth` (_Exemplu in blade `href="{{route('admin.get-oauth')}}")`_)

            ### Exemplu de implementare in `api.php`

            ```php
            Route::group(['middleware' => ['auth:api', 'api.throttled'], 'as' => 'api.auth.'], function() {
                Route::group(['namespace' => 'API\V1', 'prefix' => 'v1', 'as' => 'v1.'], function() {
                    Route::group(['prefix' => 'permissions', 'as' => 'permissions.'], function() {
                        Route::get('/all', 'API\PermissionController@getPermission')->name('all');
                        Route::post('/create', 'API\PermissionController@createPermission')->name('create');
                        Route::post('/assign/permission/role', 'API\PermissionController@assignRolePermission')->name('assignRole');
                        Route::post('/assign/permission/user', 'API\PermissionController@assignUserPermission')->name('assignUser');
                        Route::post('/revoke/permission/role', 'API\PermissionController@revokeRolePermission')->name('revokeRole');
                        Route::post('/revoke/permission/user', 'API\PermissionController@revokeUserPermission')->name('revokeUser');
                    });
                })
            })
            ```

> CUSTOM COMMANDS

-   Create command `make:view`

    -   `php artisan make:command MakeView`
    -   Contents of `app/Console/Commands/MakeView.php`

        ```php
        <?php

        namespace App\Console\Commands;

        use Illuminate\Console\Command;
        use File;

        class MakeView extends Command
        {
            /**
            * The name and signature of the console command.
            *
            * @var string
            */
            protected $signature = 'make:view {view}';

            /**
            * The console command description.
            *
            * @var string
            */
            protected $description = 'Create blade template';

            /**
            * Create a new command instance.
            *
            * @return void
            */
            public function __construct()
            {
                parent::__construct();
            }

            /**
            * Execute the console command.
            *
            * @return int
            */
            public function handle()
            {
                $view = $this->argument('view');

                $path = $this->viewPath($view);

                $this->createDir($path);

                if (File::exists($path)) {
                    $this->error("File {$path} already exists!");
                    return;
                }

                File::put($path, $path);

                $this->info("File {$path} created.");
            }

            /**
            * Get the view full path.
            *
            * @param string $view
            *
            * @return string
            */
            public function viewPath($view)
            {
                $view = str_replace('.', '/', $view) . '.blade.php';

                $path = "resources/views/{$view}";

                return $path;
            }

            /**
            * Create view directory if not exists.
            *
            * @param $path
            */
            public function createDir($path)
            {
                $dir = dirname($path);

                if (!file_exists($dir)) {
                    mkdir($dir, 0777, true);
                }
            }
        }
        ```

    -   Usage:
        -   `php artisan make:view test`
        -   `php artisan make:view asd/test`

> CI

-   Daca tot implementam testele din PHPUnit vrem sa implementam la fiecare MERGE REQUEST / COMMIT IN DEVELOP un Pipeline ce v-a executa acele teste. (_Poti aproba merge care sa fie executat doar daca Pipeline-ul nu da fail_).

-   Content of `.gitlab-ci.yml`

    ```yml
    workflow:
    rules:
        - if: $CI_MERGE_REQUEST_ID
        - if: $CI_COMMIT_BRANCH == 'develop'
    stages:
    - preparation
    - building
    - testing
    - security

    image: edbizarro/gitlab-ci-pipeline-php:7.3

    #
    # Variabilele trebuie sa fie la fel cu cele din .env.testing
    #
    # Variables
    variables:
    MYSQL_ROOT_PASSWORD: root
    MYSQL_USER: ohdear_ci
    MYSQL_PASSWORD: ohdear_secret
    MYSQL_DATABASE: ohdear_ci
    DB_HOST: mysql

    cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"

    #
    # Instalam pachetele din composer
    #
    composer:
    stage: preparation
    script:
        - php -v
        - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
        - cp .env.testing .env
        - php artisan key:generate
    artifacts:
        paths:
        - vendor/
        - .env
        expire_in: 1 days
        when: always
    cache:
        paths:
        - vendor/

    #
    # Instalam pachetele din NPM
    #
    yarn:
    stage: preparation
    script:
        - yarn --version
        - yarn install --pure-lockfile
    artifacts:
        paths:
        - node_modules/
        expire_in: 1 days
        when: always
    cache:
        paths:
        - node_modules/

    #
    # Build JS files
    #
    build-assets:
    stage: building
    # Download the artifacts for these jobs
    dependencies:
        - composer
        - yarn
    script:
        - yarn --version
        - yarn run production --progress false
    artifacts:
        paths:
        - public/css/
        - public/js/
        - public/fonts/
        - public/mix-manifest.json
        expire_in: 1 days
        when: always

    #
    # Migram baza de date si pornim seederele
    #
    db-seeding:
    stage: building
    services:
        - name: mysql:8.0
        command: ["--default-authentication-plugin=mysql_native_password"]
    # Download the artifacts for these jobs
    dependencies:
        - composer
        - yarn
    script:
        - mysql --version
        - php artisan migrate:fresh --seed
        - mysqldump --host="${DB_HOST}" --user="${MYSQL_USER}" --password="${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" > db.sql
    artifacts:
        paths:
        - storage/logs # for debugging
        - db.sql
        expire_in: 1 days
        when: always

    #
    # Dam drumul la testele de PHPUnit
    #
    phpunit:
    stage: testing
    services:
        - name: mysql:8.0
        command: ["--default-authentication-plugin=mysql_native_password"]
    # Download the artifacts for these jobs
    dependencies:
        - build-assets
        - composer
        - db-seeding
    script:
        - php -v
        - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak
        - echo "" | sudo tee /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
        - mysql --host="${DB_HOST}" --user="${MYSQL_USER}" --password="${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" < db.sql
        - ./vendor/phpunit/phpunit/phpunit --version
        - php -d short_open_tag=off ./vendor/phpunit/phpunit/phpunit -v --colors=never --stderr
        - sudo cp /usr/local/etc/php/conf.d/docker-php-ext-xdebug.bak /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
    artifacts:
        paths:
        - ./storage/logs # for debugging
        expire_in: 1 days
        when: on_failure

    codestyle:
    stage: testing
    image: lorisleiva/laravel-docker
    script:
        - phpcs --standard=PSR2 --extensions=php app
    dependencies: []

    phpcpd:
    stage: testing
    script:
        - test -f phpcpd.phar || curl -L https://phar.phpunit.de/phpcpd.phar -o phpcpd.phar
        - php phpcpd.phar app/ --min-lines=50
    dependencies: []
    cache:
        paths:
        - phpcpd.phar

    sensiolabs:
    stage: security
    script:
        - test -d security-checker || git clone https://github.com/sensiolabs/security-checker.git
        - cd security-checker
        - composer install
        - php security-checker security:check ../composer.lock
    dependencies: []
    cache:
        paths:
        - security-checker/
    ```

-   Creeam fisierul `.env.testing` care o sa fie la fel cu cel `.env` doar ca o sa aiba variabilele bazei de date definite in fisierul `.yml`

    ```php
    APP_NAME=Laravel
    APP_ENV=local
    APP_KEY=base64:6FVYhidVjII7mK6caOpj+0+Ne0iC7Q0wUSGh9QMvDGU=
    APP_DEBUG=true
    APP_URL=http://localhost

    LOG_CHANNEL=stack

    DB_CONNECTION=mysql
    DB_HOST=mysql
    DB_PORT=3306
    DB_DATABASE=ohdear_ci
    DB_USERNAME=ohdear_ci
    DB_PASSWORD=ohdear_secret

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=file
    SESSION_LIFETIME=120

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    MAIL_FROM_ADDRESS=null
    MAIL_FROM_NAME="${APP_NAME}"

    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1

    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
    ```
